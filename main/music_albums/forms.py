from unicodedata import name
from django import forms


class CreateNewMAlbum(forms.Form):
    title = forms.CharField(label="Album title", max_length=50)
    artist = forms.CharField(label="Artist", max_length=50)
    description = forms.CharField(label="Album description",
                                  widget=forms.Textarea(attrs={'class': 'ckeditor'}))
    public = forms.BooleanField(label="Make public", required=False)
    cover = forms.ImageField(label="Cover picture", required=False)


class EditMAlbum(forms.Form):
    def __init__(self, album, *args, **kwargs):
        super(EditMAlbum, self).__init__(*args, **kwargs)
        self.fields['title'] = forms.CharField(label="Album title",
                                               max_length=50,
                                               widget=forms.TextInput(
                                                   attrs={'value': album.title}
                                               ))
        self.fields['artist'] = forms.CharField(label="Artist",
                                                max_length=50,
                                                widget=forms.TextInput(
                                                    attrs={'value': album.artist}))
        self.fields['description'] = forms.CharField(label="Album description",
                                                     required=False,
                                                     max_length=250,
                                                     widget=forms.Textarea(
                                                         attrs={'value': album.description,'class': 'ckeditor'}
                                                     ))
        self.id = album.id

        if album.public:
            self.fields['public'] = forms.BooleanField(label="Make public", required=False,
                                                       widget=forms.CheckboxInput(
                                                           attrs={'checked': 'checked'}
                                                       ))
        else:
            self.fields['public'] = forms.BooleanField(label="Make public", required=False,
                                                       widget=forms.CheckboxInput())

    title = forms.CharField()
    artist = forms.CharField()
    description = forms.CharField(widget=forms.Textarea(attrs={'class': 'ckeditor'}))
    public = forms.BooleanField()
    cover = forms.ImageField(label="Cover picture", required=False)


class AddNewCredit(forms.Form):
    contributor = forms.CharField(max_length=50)
    contribution = forms.CharField(max_length=200, required=False)


class AddNewSong(forms.Form):
    title = forms.CharField(label="Song Title", max_length=50)
    artists = forms.CharField(label="Artists", max_length=100)
    description = forms.CharField(label="Description", required=False, widget=forms.Textarea(attrs={'class': 'ckeditor'}))
    isVideo=forms.BooleanField(label="Video",required=False)
    track = forms.FileField(label="Track")

    def validate_file_extension(value):
        if not value.name.endswith('.mp3'):
            return False
        if not value.name.endswith('.ogg'):
            return False
        if not value.name.endswith('.wma'):
            return False
        if not value.name.endswith('.wave'):
            return False 
        if not value.name.endswith('.mp4'):
            return False
        if not value.name.endswith('.webm'):
            return False    
        return True



class EditSong(forms.Form):
    def __init__(self, song, *args, **kwargs):
        super(EditSong, self).__init__(*args, **kwargs)
        self.fields['title'] = forms.CharField(label="Song title",
                                               widget=forms.TextInput(
                                                   attrs={'value': song.title}
                                               ),
                                               max_length=50
                                               )
        self.fields['description'] = forms.CharField(label="Description",
                                                     widget=forms.Textarea(
                                                         attrs={'placeholder': song.description,'class': 'ckeditor'}
                                                     ),
                                                     required=False,
                                                     max_length=250
                                                     )
        self.fields['artists'] = forms.CharField(label="artists",
                                                 widget=forms.TextInput(
                                                     attrs={'value': song.artists}
                                                 ),
                                                 max_length=100
                                                 )
        self.fields['order'] = forms.IntegerField(label="Order",
                                               widget=forms.NumberInput(
                                                   attrs={'value': song.order}
                                               ),
                                              
                                               )                                         
        self.id = song.id

    title = forms.CharField()
    artists = forms.CharField()
    description = forms.CharField(widget=forms.Textarea(attrs={'class': 'ckeditor'}))
    order = forms.IntegerField()
    isVideo=forms.BooleanField(label="Video",required=False)
    track = forms.FileField(label="Track", required=False)
