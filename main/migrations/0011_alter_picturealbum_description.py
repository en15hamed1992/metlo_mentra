# Generated by Django 3.2.4 on 2022-05-18 22:46

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0010_song_isvideo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='picturealbum',
            name='description',
            field=ckeditor.fields.RichTextField(blank=True, null=True),
        ),
    ]
