const SORTABLE = document.querySelector('.sortable');
slist(SORTABLE);
function slist(target) {
    // (A) SET CSS + GET ALL LIST ITEMS
    let items = target.querySelectorAll('.draggable');
    let inputs = target.querySelectorAll('.draggable>input');
    let current = null;
    order(inputs);
    // (B) MAKE ITEMS DRAGGABLE + SORTABLE
    for (let i of items) {
        // (B1) ATTACH DRAGGABLE

        i.draggable = true;

        // (B2) DRAG START - YELLOW HIGHLIGHT DROPZONES
        i.ondragstart = (ev) => {
            current = i;
            i.classList.add('dragging')
            // for (let it of items) {
            //     if (it != current) { it.classList.add("dragging"); }
            // }
        };

        // (B3) DRAG ENTER - RED HIGHLIGHT DROPZONE
        i.ondragenter = (ev) => {
            if (i != current) { i.classList.add("enter"); }
        };

        // (B4) DRAG LEAVE - REMOVE RED HIGHLIGHT
        i.ondragleave = () => {
            i.classList.remove("enter");
        };

        // (B5) DRAG END - REMOVE ALL HIGHLIGHTS
        i.ondragend = () => {
            for (let it of items) {
                it.classList.remove("dragging");
                it.classList.remove("enter");
            }
        };

        // (B6) DRAG OVER - PREVENT THE DEFAULT "DROP", SO WE CAN DO OUR OWN
        i.ondragover = (evt) => { evt.preventDefault(); };

        // (B7) ON DROP - DO SOMETHING
        i.ondrop = (evt) => {
            evt.preventDefault();
            if (i != current) {
                let currentpos = 0, droppedpos = 0;
                for (let it = 0; it < items.length; it++) {
                    if (current == items[it]) { currentpos = it; }
                    if (i == items[it]) { droppedpos = it; }
                }
                if (currentpos < droppedpos) {
                    i.parentNode.insertBefore(current, i.nextSibling);
                    const newList = target.querySelectorAll('.draggable>input');
                    order(newList)
                } else {
                    i.parentNode.insertBefore(current, i);
                    const newList = target.querySelectorAll('.draggable>input');
                    order(newList)
                }
            }
        };
    }
}
function order(list) {
    let counter = 0;
    console.log(list);
    list.forEach(element => {
        counter++
        element.setAttribute('value', counter)
    });
}