from django.http import HttpResponseForbidden, HttpResponseRedirect, JsonResponse
from django.shortcuts import redirect, render

from .forms import EditPost, CreateNewPost, AddPostCredit
from .models import Post, PostCredit
from .serializers import PostSerializer


def indexPost(response, id):
    if not response.user.is_authenticated:
        return HttpResponseRedirect('/login/')
    p = Post.objects.get(id=id)
    # if redirecting from edit
    if response.method == "POST":
        success = p.edit_post(response)
        if success:
            return HttpResponseRedirect("/api/post/")
        return HttpResponseRedirect("/api/home/")

    form = EditPost(p)
    creditForm = AddPostCredit()

    return render(response, "main/posts/post.html",
                  {"post": p, "form": form, "creditForm": creditForm})


def deletePost(response, id):
    if not response.user.is_authenticated:
        return HttpResponseRedirect('/login/')
    post=Post.objects.get(id=id)
    post.delete_post()
    return redirect('/api/post/')


def listPost(response):
    if response.user.is_authenticated:
        pList = Post.objects.all()

        return render(response, "main/posts/postList.html", {"list": pList})
    return HttpResponseRedirect('/login/')


def createPost(response):
    if not response.user.is_authenticated:
        return HttpResponseRedirect('/login/')
    if response.method == "POST":
        success = Post().create_post(response)
        if success:
            return HttpResponseRedirect("/api/post/")
        return HttpResponseRedirect("/api/home/")
    form = CreateNewPost()
    return render(response, "main/posts/postCreate.html", {"form": form})


def addPostCredit(response, id):
    if not response.user.is_authenticated:
        return HttpResponseRedirect('/login/')
    success = Post.objects.get(id=id).add_credit(response)
    if success:
        return HttpResponseRedirect("/api/post/" + str(id)+"/")
    return HttpResponseRedirect("/api/post/")
def deletePostCredit(response, id,credit_id):
    if not response.user.is_authenticated:
        return HttpResponseRedirect('/login/')
    PostCredit.objects.get(id=credit_id).delete()
    return redirect('/api/post/'+str(id)+'/')

def post(request, id):
    if request.method == 'GET':
        post = Post.objects.get(id=id)
        if not post.public:
            return None
        serializer = PostSerializer(post)
        return JsonResponse(serializer.data)
    # return JsonResponse()

def orderPosts(response):
    data=response.POST.items()
    for key,value in data:
       if(key=='csrfmiddlewaretoken'):
            continue
       post= Post.objects.get(id=key)
       post.order=int(value)
       Post.save(post)
    return HttpResponseRedirect("/api/post/")

def orderCredites(response,id):
    data=response.POST.items()
    for key,value in data:
       if(key=='csrfmiddlewaretoken'):
            continue
       credit= PostCredit.objects.get(id=key)
       credit.order=int(value)
       PostCredit.save(credit)
    return HttpResponseRedirect("/api/post/" + str(id) + "/")
   

def post_list(request):
    if request.method == 'GET':
        posts = Post.objects.filter(public=True)
        serializer = PostSerializer(posts, many=True)
        return JsonResponse(serializer.data, safe=False)
    # return JsonResponse()
