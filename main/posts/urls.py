from django.urls import path
from . import views
#from .views import PostView, CreatePostView

urlpatterns = [

    path("", views.listPost, name="listPost"),
    path("order/", views.orderPosts, name="orderPosts"),
    path("<int:id>/", views.indexPost, name="indexPost"),
    path("<int:id>/delete/", views.deletePost, name="deletePost"),
    path("create/", views.createPost, name="createPost"),
    path("<int:id>/addCredit/", views.addPostCredit, name="addPostCredit"),
    path("<int:id>/credit/<int:credit_id>/delete/", views.deletePostCredit, name="deletePostCredit"),
    path("<int:id>/credits/order/", views.orderCredites, name="orderCredits"),

    path("rest/list/", views.post_list, name="restPostList"),
    path("rest/<int:id>/", views.post, name="restSinglePost")
]